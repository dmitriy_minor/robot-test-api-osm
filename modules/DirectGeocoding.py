"""
Модуль отправляют запрос на API OSM c заданным в тесте значением локации, производит парсинг ответа, ищет совпадения по долготе и широте.
После чего принимает решение о результате прохождения теста.
"""

import sys
import json
import logging
import requests

from xml.etree import ElementTree

__author__ = 'Dmitriy Minor'
__all__ = ['DirectGeocoding', 'DGError']

logger = logging.getLogger("tester")

if sys.version_info < (3, 5):
    print("Error. Use python 3.5 or greater")
    sys.exit(1)


class DirectGeocoding:
    """
    Отправляет запрос на API OSM с известной локацией.
    Парсит вывод в зависимости от заданного формата, находит в нем координаты и сравнивает их с заданными в тесте.
    После чего принимает решение о результате прохождения теста.
    """

    URL = "https://nominatim.openstreetmap.org/search{}format={}&polygon_geojson=1&addressdetails=1"

    def passing_parameters(self, lat, lon, location, wait_result, format_):
        """
        Парсит входные данные из Robot Framework в глобальные переменные.
        """
        self._lat_value = lat
        self._lon_value = lon
        self._location = location
        self._wait_result = wait_result
        self._format = format_
        self.response = None
        self.result = None

    def sending_request(self):
        """ Отправляет запрос на API nominatim.openstreetmap.org с известной локацией"""

        response = requests.get(self.URL.format(self._location, self._format))
        if response.status_code != 200:
            logger.error(f"Не верный код ответа: {response.status_code}")
            self.result = False
            if not self._wait_result and not self.result:
                logger.info("Test SUCCESSFUL")
            else:
                raise DGError(f"Не верный код ответа: {response.status_code}")
        self.parser(response)

    def parser(self, response: requests):
        """
        На основе формата, решает какой метод с парсером вызвать.

        :param response: Ответ API OSM.
        """

        if self._format == 'xml':
            self.xml_parser(response.content)
        elif self._format == 'json':
            self.json_parser(response.content)
        elif self._format == 'jsonv2':
            self.json_parser(response.content)

    def xml_parser(self, body: bytes):
        """
        XML парсер тела ответа API OSM. Ищет в ответе значения ширины и долготы.

        :param body: Тело ответ API OSM.
        """

        search_results = []
        body_string = ElementTree.ElementTree(body.decode()).getroot()
        body_xml = ElementTree.fromstring(body_string)
        for child in body_xml:
            search_results.append(
                {
                    "lat": child.attrib.get('lat'),
                    "lon": child.attrib.get('lon')
                }
            )
        self.response = search_results

    def json_parser(self, body: bytes):
        """
        JSON парсер тела ответа API OSM. Ищет в ответе значения ширины и долготы.

        :param body: Тело ответ API OSM.
        """

        search_results = []
        body_json = json.loads(body.decode())
        for item in body_json:
            search_dict = {"lat": item.get("lat"), "lon": item.get("lon")}
            search_results.append(search_dict)
        self.response = search_results

    def coordinate_comparison(self):
        """
        Сравнивает полученные значения широты и долготы с заданными в тесте.
        Результат сравнения записывает в глобальную переменную.
        """

        logger.debug(f"Location: {self._location}")
        for num, ind in enumerate(self.response, 1):
            lat = ind.get("lat")
            lon = ind.get("lon")
            logger.debug(f'Match number {num}:')
            logger.debug(f"---| Expected value lat {self._lat_value}, received value {lat}")
            logger.debug(f"---| Expected value lon {self._lon_value}, received value {lon}")
            if str(self._lat_value) == lat and str(self._lon_value) == lon:
                logger.info("The expected value is equal to the received")
                self.result = True
                break
            self.result = False

    def comparison_of_expected_results(self):
        """ Сравнивает результат сравнения координат с требуемым результатом теста"""

        if self._wait_result and self.result:
            logger.info("Test SUCCESSFUL")
        elif not self._wait_result and not self.result:
            logger.info("Test SUCCESSFUL")
        else:
            raise DGError("Совпадение не найдено")


class DGError(Exception):
    pass
