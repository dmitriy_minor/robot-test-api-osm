"""
Модуль отправляют запрос на API OSM c заданными в тесте значениями широты и долготы, производит поиск локации в ответе.
Ищет совпадение заданной локации с той, что нашел в ответе.
После чего принимает решение о прохождении теста.
"""

import sys
import json
import logging
import requests

from xml.etree import ElementTree

__author__ = 'Минор Д.С'
__all__ = ["ReverseGeocoding", "RGError"]

logger = logging.getLogger("tester")

if sys.version_info < (3, 5):
    print("Error. Use python 3.5 or greater")
    sys.exit(1)


class ReverseGeocoding:
    """
    Отправляет запрос на API OSM с известной широтой и долготой.
    Парсит вывод в зависимости от заданного формата, находит в нем локацию и сравнивает ее с заданной в тесте.
    В зависимости от условий теста, предоставляет соответсвующий результат классу TestResult.
    """

    URL = "https://nominatim.openstreetmap.org/reverse?format={}&lat={}&lon={}&zoom={}&addressdetails=1"

    def passing_parameters(self, lat, lon, location, wait_result, zoom, format_):
        """
        Парсит входные данные из Robot Framework в глобальные переменные.
        """
        self._lat_value = lat
        self._lon_value = lon
        self._location_value = location
        self._wait_result = wait_result
        self._zoom = zoom
        self._format = format_
        self.response = None
        self.result = None

    def sending_request(self):
        """ Отправляет запрос на API nominatim.openstreetmap.org с известными координатами."""

        response = requests.get(self.URL.format(self._format, self._lat_value, self._lon_value, self._zoom))
        print(self.URL.format(self._format, self._lat_value, self._lon_value, self._zoom))
        if response.status_code != 200:
            logger.error(f"Не верный код ответа: {response.status_code}")
            self.result = False
            if not self._wait_result and not self.result:
                logger.info("Test SUCCESSFUL")
            else:
                raise RGError(f"Не верный код ответа: {response.status_code}")
            return
        self.parser(response)

    def parser(self, response: requests):
        """
        На основе формата, решает какой метод с парсером вызвать.

        :param response: Ответ API OSM.
        """

        if self._format == 'xml':
            self.xml_parser(response.content)
        elif self._format == 'json':
            self.json_parser(response.content)
        elif self._format == 'jsonv2':
            self.json_parser(response.content)
        elif self._format == 'geojson':
            self.geojson_parser(response.content)
        elif self._format == 'geocodejson':
            self.geocodejson_parser(response.content)

    def xml_parser(self, body: bytes):
        """
        XML парсер тела ответа API OSM. Ищет в ответе локацию.

        :param body: Тело ответ API OSM.
        """

        body_str = ElementTree.ElementTree(body.decode()).getroot()
        body_xml = ElementTree.fromstring(body_str)
        found_location = body_xml[0].text
        self.response = found_location

    def json_parser(self, body: bytes):
        """
        JSON парсер тела ответа API OSM. Ищет в ответе локацию.

        :param body: Тело ответ API OSM.
        """

        body_json = json.loads(body.decode())
        found_location = body_json.get("display_name")
        if found_location is None:
            found_location = body_json.get("error")
        self.response = found_location

    def geojson_parser(self, body: bytes):
        """
        GeoJSON парсер тела ответа API OSM. Ищет в ответе локацию.

        :param body: Тело ответ API OSM.
        """

        body_json = json.loads(body.decode())
        try:
            found_location = body_json.get("features")[0].get("properties").get("display_name")
        except TypeError:
            found_location = body_json.get("error")
        self.response = found_location

    def geocodejson_parser(self, body: bytes):
        """
        GeocodeJSON парсер тела ответа API OSM. Ищет в ответе локацию.

        :param body: Тело ответ API OSM.
        """

        body_json = json.loads(body.decode())
        try:
            found_location = body_json.get("features")[0].get("properties").get("geocoding").get("label")
        except TypeError:
            found_location = body_json.get("error")
        self.response = found_location

    def location_comparison(self):
        """
        Сравнивает полученное значение локации с заданной в тесте.
        Результат сравнения записывает в глобальную переменную.
        """

        logger.debug(f"Lat and Lon: {self._lat_value}:{self._lon_value}")
        logger.debug(f"---| Expected value local '{self._location_value}', received value '{self.response}'")
        if self._location_value == str(self.response):
            logger.info("The expected value is equal to the received")
            self.result = True
        else:
            self.result = False

    def comparison_of_expected_results(self):
        """ Сравнивает результат сравнения локаций с требуемым результатом теста"""

        if self._wait_result and self.result:
            logger.info("Test SUCCESSFUL")
        elif not self._wait_result and not self.result:
            logger.info("Test SUCCESSFUL")
        else:
            raise RGError("Совпадение не найдено")


class RGError(Exception):
    pass
