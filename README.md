# Robot test API OSM
В данном проекте находятся библиотеки, созданные для DDT тестирования прямого и обратного геокодирования API Open Street Map.
Все тесты написаны на Robot Framework

### Зависимости:

#### Python интерпретатор:

<ul><li>Python 3.5. или новее</li></ul>

#### Python библиотеки:
<ul>
<li>xml</li>
<li>json</li>
<li>requests</li>
<li>robotframework</li>
</ul>

#### Описание модулей:

DirectGeocoding - отправляет запрос API OSM. Получает ответ и сравнивает координаты с теми, что были переданы в тесте.

На вход требует несколько значений:
1. location - локация, которую нужно найти
2. lat - ожидаемая ширина
3. lon - ожидаемая долгота
4. format_ - формат запроса ("xml","json","jsonv2")
5. wait_result - ожидаемый результат теста

ReverseGeocoding - отправляет запрос API OSM. Получает ответ и сравнивает локацию с той, что была передана в тесте.

На вход требует несколько значений:
1. location - ожидаемая локация
2. lat - ширина
3. lon - долгота
4. format_ - формат запроса ("xml","json","jsonv2", "geojson", "geocodejson")
5. zoom - масштаб поиска
6. wait_result - ожидаемый результат теста

### Пример запуска
``````
python -m robot.run -t "1. Positive test direct geocoding OSM API." test/direct_geocode_test.robot
``````