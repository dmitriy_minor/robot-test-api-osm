*** Settings ***
Library     modules.ReverseGeocoding      WITH NAME   RG

*** Test Cases ***
1. Positive test reverse geocoding OSM API.
    [Documentation]     Send valid latitude and longitude coordinates to the OSM API, with different scales. Check the location known to us with what came in response. Expect a successful test case result.
    Entering the parameters     52.5172781          13.3978248              Deutschland                                                                                                                                                         ${TRUE}        3        xml
    Entering the parameters     52.5487921          -1.8164308339635031     England, United Kingdom                                                                                                                                             ${TRUE}        5        xml
    Entering the parameters     44.4355254          26.1157189              Municipiul București, România                                                                                                                                       ${TRUE}        10       xml
    Entering the parameters     38.005169699999996  23.72949633941048       Αδωνιδος, Άγιος Νικόλαος, Σεπόλια, Δήμος Αθηναίων, Περιφερειακή Ενότητα Κεντρικού Τομέα Αθηνών, Περιφέρεια Αττικής, Αποκεντρωμένη Διοίκηση Αττικής, 11253, Ελλάς    ${TRUE}        16       xml
    Entering the parameters     52.5172781          13.3978248              Deutschland                                                                                                                                                         ${TRUE}        3        json
    Entering the parameters     52.5487921          -1.8164308339635031     England, United Kingdom                                                                                                                                             ${TRUE}        5        json
    Entering the parameters     44.4355254          26.1157189              Municipiul București, România                                                                                                                                       ${TRUE}        10       json
    Entering the parameters     38.005169699999996  23.72949633941048       Αδωνιδος, Άγιος Νικόλαος, Σεπόλια, Δήμος Αθηναίων, Περιφερειακή Ενότητα Κεντρικού Τομέα Αθηνών, Περιφέρεια Αττικής, Αποκεντρωμένη Διοίκηση Αττικής, 11253, Ελλάς    ${TRUE}        16       json
    Entering the parameters     52.5172781          13.3978248              Deutschland                                                                                                                                                         ${TRUE}        3        jsonv2
    Entering the parameters     52.5487921          -1.8164308339635031     England, United Kingdom                                                                                                                                             ${TRUE}        5        jsonv2
    Entering the parameters     44.4355254          26.1157189              Municipiul București, România                                                                                                                                       ${TRUE}        10       jsonv2
    Entering the parameters     38.005169699999996  23.72949633941048       Αδωνιδος, Άγιος Νικόλαος, Σεπόλια, Δήμος Αθηναίων, Περιφερειακή Ενότητα Κεντρικού Τομέα Αθηνών, Περιφέρεια Αττικής, Αποκεντρωμένη Διοίκηση Αττικής, 11253, Ελλάς    ${TRUE}        16       jsonv2
    Entering the parameters     52.5172781          13.3978248              Deutschland                                                                                                                                                         ${TRUE}        3        geojson
    Entering the parameters     52.5487921          -1.8164308339635031     England, United Kingdom                                                                                                                                             ${TRUE}        5        geojson
    Entering the parameters     44.4355254          26.1157189              Municipiul București, România                                                                                                                                       ${TRUE}        10       geojson
    Entering the parameters     38.005169699999996  23.72949633941048       Αδωνιδος, Άγιος Νικόλαος, Σεπόλια, Δήμος Αθηναίων, Περιφερειακή Ενότητα Κεντρικού Τομέα Αθηνών, Περιφέρεια Αττικής, Αποκεντρωμένη Διοίκηση Αττικής, 11253, Ελλάς    ${TRUE}        16       geojson
    Entering the parameters     52.5172781          13.3978248              Deutschland                                                                                                                                                         ${TRUE}        3        geocodejson
    Entering the parameters     52.5487921          -1.8164308339635031     England, United Kingdom                                                                                                                                             ${TRUE}        5        geocodejson
    Entering the parameters     44.4355254          26.1157189              Municipiul București, România                                                                                                                                       ${TRUE}        10       geocodejson
    Entering the parameters     38.005169699999996  23.72949633941048       Αδωνιδος, Άγιος Νικόλαος, Σεπόλια, Δήμος Αθηναίων, Περιφερειακή Ενότητα Κεντρικού Τομέα Αθηνών, Περιφέρεια Αττικής, Αποκεντρωμένη Διοίκηση Αττικής, 11253, Ελλάς    ${TRUE}        16       geocodejson

2. Negative test reverse geocoding OSM API use big number.
    [Documentation]     Send coordinates with a large number of digits to the OSM API. Expect the location to be Unable to geocode. The test is successful.
    Entering the parameters     52513232720765          13.397834399325461     Unable to geocode       ${TRUE}     3        xml
    Entering the parameters     52.5487921              657567756765736763     Unable to geocode       ${TRUE}     18       xml
    Entering the parameters     552.51720765435534      113397834399325461     Unable to geocode       ${TRUE}     10       xml
    Entering the parameters     -52.54879235432521      18164308339635031      Unable to geocode       ${TRUE}     16       xml
    Entering the parameters     52513232720765          13.397834399325461     Unable to geocode       ${TRUE}     3        json
    Entering the parameters     52.5487921              657567756765736763     Unable to geocode       ${TRUE}     18       json
    Entering the parameters     552.51720765435534      113397834399325461     Unable to geocode       ${TRUE}     10       json
    Entering the parameters     -52.54879235432521      18164308339635031      Unable to geocode       ${TRUE}     16       json
    Entering the parameters     52513232720765          13.397834399325461     Unable to geocode       ${TRUE}     3        jsonv2
    Entering the parameters     52.5487921              657567756765736763     Unable to geocode       ${TRUE}     18       jsonv2
    Entering the parameters     552.51720765435534      113397834399325461     Unable to geocode       ${TRUE}     10       jsonv2
    Entering the parameters     -52.54879235432521      18164308339635031      Unable to geocode       ${TRUE}     16       jsonv2
    Entering the parameters     52513232720765          13.397834399325461     Unable to geocode       ${TRUE}     3        geojson
    Entering the parameters     52.5487921              657567756765736763     Unable to geocode       ${TRUE}     18       geojson
    Entering the parameters     552.51720765435534      113397834399325461     Unable to geocode       ${TRUE}     10       geojson
    Entering the parameters     -52.54879235432521      18164308339635031      Unable to geocode       ${TRUE}     16       geojson
    Entering the parameters     52513232720765          13.397834399325461     Unable to geocode       ${TRUE}     3        geocodejson
    Entering the parameters     52.5487921              657567756765736763     Unable to geocode       ${TRUE}     18       geocodejson
    Entering the parameters     552.51720765435534      113397834399325461     Unable to geocode       ${TRUE}     10       geocodejson
    Entering the parameters     -52.54879235432521      18164308339635031      Unable to geocode       ${TRUE}     16       geocodejson

3. Negative test reverse geocoding OSM API letters.
    [Documentation]     Send coordinates containing letters. Expect a response code 400. The test is successful.
    Entering the parameters     5f3          13.397834399325461     Unable to geocode       ${FALSE}     3        xml
    Entering the parameters     52.5487921   6h6                    Unable to geocode       ${FALSE}     18       xml
    Entering the parameters     54b34        113397834399325461     Unable to geocode       ${FALSE}     10       xml
    Entering the parameters     34           7k38                   Unable to geocode       ${FALSE}     16       xml
    Entering the parameters     5f3          13.397834399325461     Unable to geocode       ${FALSE}     3        json
    Entering the parameters     52.5487921   6h6                    Unable to geocode       ${FALSE}     18       json
    Entering the parameters     54b34        113397834399325461     Unable to geocode       ${FALSE}     10       json
    Entering the parameters     34           7k38                   Unable to geocode       ${FALSE}     16       json
    Entering the parameters     5f3          13.397834399325461     Unable to geocode       ${FALSE}     3        jsonv2
    Entering the parameters     52.5487921   6h6                    Unable to geocode       ${FALSE}     18       jsonv2
    Entering the parameters     54b34        113397834399325461     Unable to geocode       ${FALSE}     10       jsonv2
    Entering the parameters     34           7k38                   Unable to geocode       ${FALSE}     16       jsonv2
    Entering the parameters     5f3          13.397834399325461     Unable to geocode       ${FALSE}     3        geojson
    Entering the parameters     52.5487921   6h6                    Unable to geocode       ${FALSE}     18       geojson
    Entering the parameters     54b34        113397834399325461     Unable to geocode       ${FALSE}     10       geojson
    Entering the parameters     34           7k38                   Unable to geocode       ${FALSE}     16       geojson
    Entering the parameters     5f3          13.397834399325461     Unable to geocode       ${FALSE}     3        geocodejson
    Entering the parameters     52.5487921   6h6                    Unable to geocode       ${FALSE}     18       geocodejson
    Entering the parameters     54b34        113397834399325461     Unable to geocode       ${FALSE}     10       geocodejson
    Entering the parameters     34           7k38                   Unable to geocode       ${FALSE}     16       geocodejson

*** Keywords ***
Entering the parameters
    [Arguments]           ${LAT}      ${LON}  ${LOCATION}     ${WAIT_RESULT}  ${ZOOM}   ${FORMAT}
    Passing parameters    ${LAT}      ${LON}  ${LOCATION}     ${WAIT_RESULT}  ${ZOOM}   ${FORMAT}
    Sending request
    Location comparison
    Comparison ofexpected results
