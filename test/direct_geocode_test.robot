*** Settings ***
Library     modules.DirectGeocoding      WITH NAME   DG

*** Test Cases ***
1. Positive test direct geocoding OSM API.
    [Documentation]     Send a list of real locations in different valid variations to the OSM API. Compare the obtained coordinates with the given ones. Expect a successful test case result.
    Entering the parameters     52.5172781          13.3978248              /Unter%20den%20Linden%201%20Berlin?                 ${TRUE}        xml
    Entering the parameters     52.5487921          -1.8164308339635031     ?q=135+pilkington+avenue,+birmingham&               ${TRUE}        xml
    Entering the parameters     44.4355254          26.1157189              ?q=17+Strada+Pictor+Alexandru+Romano%2C+Bukarest&   ${TRUE}        xml
    Entering the parameters     38.005169699999996  23.72949633941048       ?q=Αγία+Τριάδα%2C+Αδωνιδος%2C+Athens%2C+Greece&     ${TRUE}        xml
    Entering the parameters     52.5172781          13.3978248              /Unter%20den%20Linden%201%20Berlin?                 ${TRUE}        json
    Entering the parameters     52.5487921          -1.8164308339635031     ?q=135+pilkington+avenue,+birmingham&               ${TRUE}        json
    Entering the parameters     44.4355254          26.1157189              ?q=17+Strada+Pictor+Alexandru+Romano%2C+Bukarest&   ${TRUE}        json
    Entering the parameters     38.005169699999996  23.72949633941048       ?q=Αγία+Τριάδα%2C+Αδωνιδος%2C+Athens%2C+Greece&     ${TRUE}        json
    Entering the parameters     52.5172781          13.3978248              /Unter%20den%20Linden%201%20Berlin?                 ${TRUE}        jsonv2
    Entering the parameters     52.5487921          -1.8164308339635031     ?q=135+pilkington+avenue,+birmingham&               ${TRUE}        jsonv2
    Entering the parameters     44.4355254          26.1157189              ?q=17+Strada+Pictor+Alexandru+Romano%2C+Bukarest&   ${TRUE}        jsonv2
    Entering the parameters     38.005169699999996  23.72949633941048       ?q=Αγία+Τριάδα%2C+Αδωνιδος%2C+Athens%2C+Greece&     ${TRUE}        jsonv2

2. Negative test direct geocoding OSM API.
    [Documentation]     Send a non-existing location to the OSM API in different valid variations. We compare the obtained coordinates with the given ones. The coordinates must not be found. We expect a successful test case result.
    Entering the parameters     52.5172781          13.3978248              /Berrfdings?                        ${FALSE}        xml
    Entering the parameters     52.5487921          -1.8164308339635031     ?q=135+pifdsf+avenue,+bgfrig&       ${FALSE}        xml
    Entering the parameters     44.4355254          26.1157189              ?q=17+Strafd%2C+Fail&               ${FALSE}        xml
    Entering the parameters     38.005169699999996  23.72949633941048       ?q=Αδία+Τωνιδα%2C+Αδthens%2C&       ${FALSE}        xml
    Entering the parameters     52.5172781          13.3978248              /Berrfdings?                        ${FALSE}        json
    Entering the parameters     52.5487921          -1.8164308339635031     ?q=135+pifdsf+avenue,+bgfrig&       ${FALSE}        json
    Entering the parameters     44.4355254          26.1157189              ?q=17+Strafd%2C+Fail&               ${FALSE}        json
    Entering the parameters     38.005169699999996  23.72949633941048       ?q=Αδία+Τωνιδα%2C+Αδthens%2C&       ${FALSE}        json
    Entering the parameters     52.5172781          13.3978248              /Berrfdings?                        ${FALSE}        jsonv2
    Entering the parameters     52.5487921          -1.8164308339635031     ?q=135+pifdsf+avenue,+bgfrig&       ${FALSE}        jsonv2
    Entering the parameters     44.4355254          26.1157189              ?q=17+Strafd%2C+Fail&               ${FALSE}        jsonv2
    Entering the parameters     38.005169699999996  23.72949633941048       ?q=Αδία+Τωνιδα%2C+Αδthens%2C&       ${FALSE}        jsonv2

*** Keywords ***
Entering the parameters
    [Arguments]           ${LAT}      ${LON}  ${LOCATION}     ${WAIT_RESULT}  ${FORMAT}
    Passing parameters    ${LAT}      ${LON}  ${LOCATION}     ${WAIT_RESULT}  ${FORMAT}
    Sending request
    Coordinate comparison
    Comparison ofexpected results
